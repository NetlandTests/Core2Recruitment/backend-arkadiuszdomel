﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SIENN.Domain.Data.Model;

namespace SEINN.Data.EntityFramework.Configuration
{
    public class CategoryConfiguration : IEntityTypeConfiguration<Category>
    {
        public void Configure(EntityTypeBuilder<Category> builder)
        {
            builder.ToTable("Categories");

            builder.HasKey(x => x.Code);

            builder.Property(x => x.Code)
                .IsRequired()
                .HasColumnName("Code")
                .HasColumnType("varchar(255)");

            builder.Property(x => x.Description)
                .HasColumnName("Description")
                .HasColumnType("text");
        }
    }
}
