﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SIENN.Domain.Data.Model;

namespace SEINN.Data.EntityFramework.Configuration
{
    class ProductConfiguration : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.ToTable("Products");

            builder.HasKey(x => x.Code);

            builder.Property(x => x.Code)
                .IsRequired()
                .HasColumnName("Code")
                .HasColumnType("varchar(255)");

            builder.Property(x => x.Description)
                .HasColumnName("Description")
                .HasColumnType("text");

            builder.Property(x => x.Price)
                .IsRequired()
                .HasColumnType("decimal(7,2)");

            builder.Property(x => x.IsAvailabale)
                .HasColumnName("IsAvailable")
                .HasColumnType("Boolean");

            builder.Property(x => x.DeliveryDate)
                .IsRequired()
                .HasColumnName("DeliveryDate")
                .HasColumnType("date");

            builder.HasOne(x => x.Type)
                .WithMany()
                .HasForeignKey("TypeCode")
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasOne(x => x.Unit)
                .WithMany()
                .HasForeignKey("UnitCode")
                .IsRequired()
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
