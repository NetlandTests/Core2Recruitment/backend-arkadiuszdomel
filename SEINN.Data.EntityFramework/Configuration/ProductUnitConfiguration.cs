﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SIENN.Domain.Data.Model;

namespace SEINN.Data.EntityFramework.Configuration
{
    public class ProductUnitConfiguration : IEntityTypeConfiguration<ProductUnit>
    {
        public void Configure(EntityTypeBuilder<ProductUnit> builder)
        {
            builder.ToTable("Units");

            builder.HasKey(x => x.Code);

            builder.Property(x => x.Code)
                .IsRequired()
                .HasColumnName("Code")
                .HasColumnType("varchar(255)");

            builder.Property(x => x.Description)
                .HasColumnName("Description")
                .HasColumnType("text");
        }
    }
}
