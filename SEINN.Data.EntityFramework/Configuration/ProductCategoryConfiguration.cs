﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SIENN.Domain.Data.Model;

namespace SEINN.Data.EntityFramework.Configuration
{
    public class ProductCategoryConfiguration : IEntityTypeConfiguration<ProductCategory>
    {
        public void Configure(EntityTypeBuilder<ProductCategory> builder)
        {
            builder.ToTable("ProductsCategories");

            builder.HasKey(x => new { x.ProductCode, x.CategoryCode });

            builder.HasOne(x => x.Product)
                .WithMany(x => x.Categories)
                .HasForeignKey("ProductCode")
                .OnDelete(DeleteBehavior.Cascade);

            builder.HasOne(x => x.Category)
                .WithMany(x => x.Products)
                .HasForeignKey("CategoryCode")
                .OnDelete(DeleteBehavior.Restrict);
        }
    }
}
