﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace SEINN.Data.EntityFramework.Migrations
{
    public partial class ProductRelationship : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ProductsCategories_Categories_CategoryId",
                table: "ProductsCategories");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductsCategories_Products_ProductId",
                table: "ProductsCategories");

            migrationBuilder.DropForeignKey(
                name: "FK_Types_Products_Code",
                table: "Types");

            migrationBuilder.DropForeignKey(
                name: "FK_Units_Products_Code",
                table: "Units");

            migrationBuilder.DropIndex(
                name: "IX_ProductsCategories_CategoryId",
                table: "ProductsCategories");

            migrationBuilder.DropIndex(
                name: "IX_ProductsCategories_ProductId",
                table: "ProductsCategories");

            migrationBuilder.DropColumn(
                name: "CategoryId",
                table: "ProductsCategories");

            migrationBuilder.DropColumn(
                name: "ProductId",
                table: "ProductsCategories");

            migrationBuilder.AlterColumn<string>(
                name: "CategoryCode",
                table: "ProductsCategories",
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "ProductCode",
                table: "ProductsCategories",
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<string>(
                name: "TypeCode",
                table: "Products",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<string>(
                name: "UnitCode",
                table: "Products",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateIndex(
                name: "IX_ProductsCategories_CategoryCode",
                table: "ProductsCategories",
                column: "CategoryCode");

            migrationBuilder.CreateIndex(
                name: "IX_Products_TypeCode",
                table: "Products",
                column: "TypeCode");

            migrationBuilder.CreateIndex(
                name: "IX_Products_UnitCode",
                table: "Products",
                column: "UnitCode");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Types_TypeCode",
                table: "Products",
                column: "TypeCode",
                principalTable: "Types",
                principalColumn: "Code",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Products_Units_UnitCode",
                table: "Products",
                column: "UnitCode",
                principalTable: "Units",
                principalColumn: "Code",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductsCategories_Categories_CategoryCode",
                table: "ProductsCategories",
                column: "CategoryCode",
                principalTable: "Categories",
                principalColumn: "Code",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductsCategories_Products_ProductCode",
                table: "ProductsCategories",
                column: "ProductCode",
                principalTable: "Products",
                principalColumn: "Code",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_Types_TypeCode",
                table: "Products");

            migrationBuilder.DropForeignKey(
                name: "FK_Products_Units_UnitCode",
                table: "Products");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductsCategories_Categories_CategoryCode",
                table: "ProductsCategories");

            migrationBuilder.DropForeignKey(
                name: "FK_ProductsCategories_Products_ProductCode",
                table: "ProductsCategories");

            migrationBuilder.DropIndex(
                name: "IX_ProductsCategories_CategoryCode",
                table: "ProductsCategories");

            migrationBuilder.DropIndex(
                name: "IX_Products_TypeCode",
                table: "Products");

            migrationBuilder.DropIndex(
                name: "IX_Products_UnitCode",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "TypeCode",
                table: "Products");

            migrationBuilder.DropColumn(
                name: "UnitCode",
                table: "Products");

            migrationBuilder.AlterColumn<string>(
                name: "CategoryCode",
                table: "ProductsCategories",
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "ProductCode",
                table: "ProductsCategories",
                nullable: false,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<string>(
                name: "CategoryId",
                table: "ProductsCategories",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "ProductId",
                table: "ProductsCategories",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_ProductsCategories_CategoryId",
                table: "ProductsCategories",
                column: "CategoryId");

            migrationBuilder.CreateIndex(
                name: "IX_ProductsCategories_ProductId",
                table: "ProductsCategories",
                column: "ProductId");

            migrationBuilder.AddForeignKey(
                name: "FK_ProductsCategories_Categories_CategoryId",
                table: "ProductsCategories",
                column: "CategoryId",
                principalTable: "Categories",
                principalColumn: "Code",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_ProductsCategories_Products_ProductId",
                table: "ProductsCategories",
                column: "ProductId",
                principalTable: "Products",
                principalColumn: "Code",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Types_Products_Code",
                table: "Types",
                column: "Code",
                principalTable: "Products",
                principalColumn: "Code",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Units_Products_Code",
                table: "Units",
                column: "Code",
                principalTable: "Products",
                principalColumn: "Code",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
