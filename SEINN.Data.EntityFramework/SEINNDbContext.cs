﻿using Microsoft.EntityFrameworkCore;
using SEINN.Data.EntityFramework.Configuration;
using SIENN.Domain.Data.Model;

namespace SEINN.Data.EntityFramework
{
    public class SEINNDbContext : DbContext
    {
        public SEINNDbContext(DbContextOptions options) : base(options)
        {
            
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new CategoryConfiguration());
            modelBuilder.ApplyConfiguration(new ProductConfiguration());
            modelBuilder.ApplyConfiguration(new ProductTypeConfiguration());
            modelBuilder.ApplyConfiguration(new ProductUnitConfiguration());
            modelBuilder.ApplyConfiguration(new ProductCategoryConfiguration());

            base.OnModelCreating(modelBuilder);
        }

        public DbSet<Product> Products { get; set; }

        public DbSet<Category> Categories { get; set; }

        public DbSet<ProductType> ProductTypes { get; set; }

        public DbSet<ProductUnit> ProductUnits { get; set; }
    }
}
