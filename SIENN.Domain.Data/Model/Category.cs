﻿using System.Collections.Generic;

namespace SIENN.Domain.Data.Model
{
    public class Category
    {
        public string Code { get; set; }

        public string Description { get; set; }

        public virtual ICollection<ProductCategory> Products { get; set; }
    }
}
