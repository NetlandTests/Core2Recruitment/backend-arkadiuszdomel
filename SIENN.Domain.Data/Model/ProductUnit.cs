﻿namespace SIENN.Domain.Data.Model
{
    public class ProductUnit
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
