﻿namespace SIENN.Domain.Data.Model
{
    public class ProductType
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
