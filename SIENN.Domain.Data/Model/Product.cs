﻿using System;
using System.Collections.Generic;

namespace SIENN.Domain.Data.Model
{
    public class Product
    {
        public string Code { get; set; }

        public string Description { get; set; }

        public decimal Price { get; set; }

        public bool IsAvailabale { get; set; }

        public DateTime DeliveryDate { get; set; }

        public virtual ICollection<ProductCategory> Categories { get; set; }

        public ProductUnit Unit { get; set; }

        public ProductType Type { get; set; }
    }
}
