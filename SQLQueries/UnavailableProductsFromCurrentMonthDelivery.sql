﻿--This query works in postgresql

SELECT p."Code", p."DeliveryDate", p."Description", p."IsAvailable", p."Price", p."TypeCode", p."UnitCode"
FROM public."Products" as p
WHERE p."IsAvailable" = FALSE 
AND DATE_TRUNC('month', "DeliveryDate") = DATE_TRUNC('month', CURRENT_DATE);