﻿--This query works in postgresql

SELECT c."Code", c."Description", COUNT(p."Code") as productCount, AVG(p."Price") as avgPrice
	FROM public."Categories" as c
    INNER JOIN public."ProductsCategories" as pc on (pc."CategoryCode" = c."Code")
    INNER JOIN public."Products" as p on (pc."ProductCode" = p."Code")
    WHERE p."IsAvailable" = TRUE
    GROUP BY c."Code", c."Description"
    ORDER BY avgPrice DESC LIMIT 3;
    