﻿--This query works in postgresql

SELECT p."Code", p."DeliveryDate", p."Description", p."IsAvailable", p."Price", p."TypeCode", p."UnitCode"
	FROM public."Products" as p
    INNER JOIN public."ProductsCategories" as categories on (categories."ProductCode" = p."Code")
    WHERE p."IsAvailable" = TRUE
    GROUP BY p."Code", p."DeliveryDate", p."Description", p."IsAvailable", p."Price", p."TypeCode", p."UnitCode"
    HAVING(COUNT(categories."CategoryCode") > 1)