﻿namespace SIENN.Domain.Core.Exception
{
    public class SIENNException : System.Exception
    {
        public SIENNException(SIENNErrorCode code)
            : this(code, null, null)
        {
        }

        public SIENNException(SIENNErrorCode code, string message)
            : this(code, message, null)
        {
        }

        public SIENNException(SIENNErrorCode code, string message, System.Exception inner)
            : base(message, inner)
        {
            Code = code;
        }

        public SIENNErrorCode Code { get; }
    }
}
