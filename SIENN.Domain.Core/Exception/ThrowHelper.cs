﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SIENN.Domain.Core.Exception
{

    public class ThrowHelper
    {
        public static SIENNException Create(SIENNErrorCode code)
        {
            return new SIENNException(code);
        }

        public static SIENNException Create(SIENNErrorCode code, string msg)
        {
            return new SIENNException(code, msg);
        }

        public static void ThrowIfNull(object obj, SIENNErrorCode code, string msg)
        {
            if (obj == null)
            {
                throw Create(code, msg);
            }
        }

        public static void ThrowIfNull(object obj, SIENNErrorCode code)
        {
            if (obj == null)
            {
                throw Create(code);
            }
        }

        public static void ThrowIfNull(string obj, SIENNErrorCode code)
        {
            if (string.IsNullOrEmpty(obj))
            {
                throw Create(code);
            }
        }
    }
}
