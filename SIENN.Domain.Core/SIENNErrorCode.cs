﻿namespace SIENN.Domain.Core
{
    public enum SIENNErrorCode
    {
        ProductExists,
        ProductTypeExists,
        ProductUnitExists,
        CategoryExists,
        ProductNotFound,
        ProductTypeNotFound,
        ProductUnitNotFound,
        CategoryNotFount,
        UnexpectedError
    }
}
