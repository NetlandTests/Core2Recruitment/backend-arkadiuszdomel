﻿using Microsoft.AspNetCore.Mvc;
using SIENN.Domain.Data.Transfer.ProductUnit;
using SIENN.Services.Interfaces;
using System.Threading.Tasks;

namespace SIENN.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Unit")]
    public class UnitController : Controller
    {
        private readonly IProductUnitService _productUnitService;
        
        public UnitController(IProductUnitService productUnitService)
        {
            _productUnitService = productUnitService;
        }


        [HttpGet]
        public async Task<IActionResult> List()
        {
            var list = await _productUnitService.ListAsync();

            return Json(list);
        }


        [HttpGet("{code}")]
        public async Task<IActionResult> Get(string code)
        {
            var unit = await _productUnitService.GetAsync(code);

            return Json(unit);
        }
        
        
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]CreateProductUnitCommand command)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest();
            }

            await _productUnitService.CreateAsync(command);

            return Ok();
        }
        
 
        [HttpPut("{code}")]
        public async Task<IActionResult> Put(string code, [FromBody]UpdateProductUnitCommand command)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }

            await _productUnitService.UpdateAsync(code, command);

            return Ok();
        }
        

        [HttpDelete("{code}")]
        public async Task<IActionResult> Delete(string code)
        {
            await _productUnitService.DeleteAsync(code);

            return Ok();
        }
    }
}
