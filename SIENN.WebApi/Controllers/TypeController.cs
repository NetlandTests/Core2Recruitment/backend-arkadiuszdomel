﻿using Microsoft.AspNetCore.Mvc;
using SIENN.Domain.Data.Transfer.ProductType;
using SIENN.Services.Interfaces;
using System.Threading.Tasks;

namespace SIENN.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Type")]
    public class TypeController : Controller
    {
        private readonly IProductTypeService _productTypeService;

        public TypeController(IProductTypeService productTypeService)
        {
            _productTypeService = productTypeService;
        }
        

        [HttpGet]
        public async Task<IActionResult> List()
        {
            var list = await _productTypeService.ListAsync();

            return Json(list);
        }


        [HttpGet("{code}")]
        public async Task<IActionResult> Get(string code)
        {
            var type = await _productTypeService.GetAsync(code);

            return Json(type);
        }
        

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]CreateProductTypeCommand command)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest();
            }

            await _productTypeService.CreateAsync(command);

            return Ok();
        }
        

        [HttpPut("{code}")]
        public async Task<IActionResult> Put(string code, [FromBody]UpdateProductTypeCommand command)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest();
            }

            await _productTypeService.UpdateAsync(code, command);

            return Ok();
        }
        

        [HttpDelete("{code}")]
        public async Task<IActionResult> Delete(string code)
        {
            await _productTypeService.DeleteAsync(code);

            return Ok();
        }
    }
}
