﻿using Microsoft.AspNetCore.Mvc;
using SIENN.Domain.Data.Transfer.Category;
using SIENN.Services.Interfaces;
using System.Threading.Tasks;

namespace SIENN.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Category")]
    public class CategoryController : Controller
    {
        private readonly ICategoryService _categoryService;

        public CategoryController(ICategoryService categoryService)
        {
            _categoryService = categoryService;
        }


        [HttpGet]
        public async Task<IActionResult> List()
        {
            var category = await _categoryService.ListAsync();

            return Json(category);
        }


        [HttpGet("{code}")]
        public async Task<IActionResult> Get(string code)
        {
            var category = await _categoryService.GetAsync(code);

            return Json(category);
        }
        

        [HttpPost]
        public async Task<IActionResult> Post([FromBody]CreateCategoryCommand command)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest();
            }

            await _categoryService.CreateAsync(command);

            return Ok();
        }
        

        [HttpPut("{code}")]
        public async Task<IActionResult> Put(string code, [FromBody]UpdateCategoryCommand command)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest();
            }

            await _categoryService.UpdateAsync(code, command);

            return Ok();

        }
        

        [HttpDelete("{code}")]
        public async Task<IActionResult> Delete(string code)
        {
            await _categoryService.DeleteAsync(code);

            return Ok();
        }
    }
}
