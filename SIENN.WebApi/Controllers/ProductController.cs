﻿using Microsoft.AspNetCore.Mvc;
using SIENN.Domain.Data.Transfer.Product;
using SIENN.Services.Interfaces;
using System.Threading.Tasks;

namespace SIENN.WebApi.Controllers
{
    [Produces("application/json")]
    [Route("api/Product")]
    public class ProductController : Controller
    {
        private readonly IProductService _productService;

        public ProductController(IProductService productService)
        {
            _productService = productService;
        }


        [HttpGet]
        public async Task<IActionResult> List()
        {
            var list = await _productService.ListAsync();

            return Json(list);
        }


        [HttpGet("{code}")]
        public async Task<IActionResult> Get(string code)
        {
            var product = await _productService.GetAsync(code);

            return Json(product);
        }

        [HttpGet("/Details/{code}")]
        public async Task<IActionResult> Details(string code)
        {
            var product = await _productService.GetDetailsAsync(code);

            return Json(product);
        }


        [HttpPost]
        public async Task<IActionResult> Post([FromBody]CreateProductCommand command)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest();
            }

            await _productService.CreateAsync(command);

            return Ok();
        }
        

        [HttpPut("{code}")]
        public async Task<IActionResult> Put(string code, [FromBody]UpdateProductCommand command)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest();
            }

            await _productService.UpdateAsync(code, command);

            return Ok();
        }
        

        [HttpDelete("{code}")]
        public async Task<IActionResult> Delete(string code)
        {
            await _productService.DeleteAsync(code);

            return Ok();
        }

        [HttpGet("/OnlyAvailable")]
        public async Task<IActionResult> GetOnlyAvailableAsync([FromQuery]int currentPage, [FromQuery]int pageSize)
        {
            var products = await _productService.GetOnlyAvailableProductsAsync(currentPage, pageSize);

            return Json(products);
        }

        [HttpGet("/GetFiltered")]
        public async Task<IActionResult> GetFilteredAsync([FromQuery]int currentPage, [FromQuery]int pageSize, [FromQuery]string categoryCode, [FromQuery]string typeCode, [FromQuery]string unitCode)
        {
            var products = await _productService.GetFilteredProductsAsync(currentPage, pageSize, categoryCode, typeCode, unitCode);

            return Json(products);
        }
    }
}
