﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using SIENN.Domain.Core;
using SIENN.Domain.Core.Exception;
using SIENN.Domain.Data.Transfer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIENN.WebApi.Attributes
{
    public class ExceptionFilter : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            if (context.Exception.GetType() == typeof(SIENNException))
            {
                if (context.Exception is SIENNException siennException)
                {
                    context.Result = new BadRequestObjectResult(new ErrorDto
                    {
                        Code = siennException.Code,
                        Message = siennException.Message
                    });
                }
            }
            else
            {
                context.Result = new BadRequestObjectResult(new ErrorDto
                {
                    Code = SIENNErrorCode.UnexpectedError,
                    Message = "Unexpected error"
                });
            }
        }

    }
}
