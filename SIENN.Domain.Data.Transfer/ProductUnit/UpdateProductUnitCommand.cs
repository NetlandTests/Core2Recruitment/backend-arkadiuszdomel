﻿namespace SIENN.Domain.Data.Transfer.ProductUnit
{
    public class UpdateProductUnitCommand
    {
        public string Description { get; set; }
    }
}
