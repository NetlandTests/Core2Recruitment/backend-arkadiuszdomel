﻿using System.ComponentModel.DataAnnotations;

namespace SIENN.Domain.Data.Transfer.ProductUnit
{
    public class CreateProductUnitCommand
    {
        [Required]
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
