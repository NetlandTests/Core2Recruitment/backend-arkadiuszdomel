﻿namespace SIENN.Domain.Data.Transfer.ProductUnit
{
    public class ProductUnitVm
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
