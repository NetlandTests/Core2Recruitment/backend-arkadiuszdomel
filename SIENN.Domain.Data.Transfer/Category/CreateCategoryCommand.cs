﻿using System.ComponentModel.DataAnnotations;

namespace SIENN.Domain.Data.Transfer.Category
{
    public class CreateCategoryCommand
    {
        [Required]
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
