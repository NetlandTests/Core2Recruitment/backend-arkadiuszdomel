﻿namespace SIENN.Domain.Data.Transfer.Category
{
    public class UpdateCategoryCommand 
    {
        public string Description { get; set; }
    }
}
