﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SIENN.Domain.Data.Transfer.Category
{
    public class CategoryVm
    {
        public string Code { get; set; }

        public string Description { get; set; }
    }
}
