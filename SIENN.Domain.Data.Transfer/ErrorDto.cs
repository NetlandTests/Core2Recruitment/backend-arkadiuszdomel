﻿using SIENN.Domain.Core;

namespace SIENN.Domain.Data.Transfer
{
    public class ErrorDto
    {
        public SIENNErrorCode Code { get; set; }
        public string Message { get; set; }

    }
}
