﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SIENN.Domain.Data.Transfer
{
    public class PaginationResultVm<T>
    {
        public int Page { get; set; }
        public int Size { get; set; }
        public List<T> Items { get; set; }
        public int TotalCount { get; set; }
    }
}
