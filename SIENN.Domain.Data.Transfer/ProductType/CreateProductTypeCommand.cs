﻿using System.ComponentModel.DataAnnotations;

namespace SIENN.Domain.Data.Transfer.ProductType
{
    public class CreateProductTypeCommand
    {
        [Required]
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
