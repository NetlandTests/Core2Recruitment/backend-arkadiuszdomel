﻿namespace SIENN.Domain.Data.Transfer.ProductType
{
    public class ProductTypeVm
    {
        public string Code { get; set; }
        public string Description { get; set; }
    }
}
