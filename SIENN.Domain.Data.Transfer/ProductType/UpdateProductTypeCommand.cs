﻿namespace SIENN.Domain.Data.Transfer.ProductType
{
    public class UpdateProductTypeCommand
    {
        public string Description { get; set; }
    }
}
