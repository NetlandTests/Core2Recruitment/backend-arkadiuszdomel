﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SIENN.Domain.Data.Transfer.Product
{
    public class CreateProductCommand
    {
        [Required]
        public string Code { get; set; }

        public string Description { get; set; }

        public List<string> Categories { get; set; }

        [Required]
        public decimal Price { get; set; }

        [Required]
        public bool IsAvailabale { get; set; }

        [Required]
        public DateTime DeliveryDate { get; set; }

        [Required]
        public string Unit { get; set; }

        [Required]
        public string Type { get; set; }
    }
}
