﻿using System;
using System.Collections.Generic;

namespace SIENN.Domain.Data.Transfer.Product
{
    public class UpdateProductCommand
    {
        public string Description { get; set; }

        public List<string> Categories { get; set; }

        public decimal Price { get; set; }

        public bool IsAvailabale { get; set; }

        public DateTime DeliveryDate { get; set; }

        public string Unit { get; set; }

        public string Type { get; set; }
    }
}
