﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SIENN.Domain.Data.Transfer.Product
{
    public class ProductDetailsVm
    {
        public string ProductDescription { get; set; }
        public string Price { get; set; }
        public string IsAvailable { get; set; }
        public string DeliveryDate { get; set; }
        public int CategoriesCount { get; set; }
        public string Unit { get; set; }
        public string Type { get; set; }
    }
}
