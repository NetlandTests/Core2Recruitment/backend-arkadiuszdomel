﻿using SIENN.Domain.Data.Transfer.Category;
using SIENN.Domain.Data.Transfer.ProductType;
using SIENN.Domain.Data.Transfer.ProductUnit;
using System.Collections.Generic;

namespace SIENN.Domain.Data.Transfer.Product
{
    public class ProductVm
    {
        public string Code { get; set; }

        public string Description { get; set; }

        public decimal Price { get; set; }

        public bool IsAvailabale { get; set; }

        public string DeliveryDate { get; set; }

        public List<CategoryVm> Categories { get; set; }

        public ProductUnitVm Unit { get; set; }

        public ProductTypeVm Type { get; set; }
    }
}
