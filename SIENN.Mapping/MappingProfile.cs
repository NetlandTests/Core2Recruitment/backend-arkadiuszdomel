﻿using AutoMapper;
using SIENN.Domain.Data.Transfer.ProductUnit;
using SIENN.Domain.Data.Model;
using SIENN.Domain.Data.Transfer.Category;
using SIENN.Domain.Data.Transfer.Product;
using SIENN.Domain.Data.Transfer.ProductType;
using System.Linq;
using System.Globalization;

namespace SIENN.Mapping
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            MapProductType();
            MapProductUnit();
            MapCategory();
            MapProduct();
        }


        private void MapProduct()
        {
            CreateMap<Product, ProductVm>()
                .ForMember(x => x.Code, x => x.MapFrom(y => y.Code))
                .ForMember(x => x.Description, x => x.MapFrom(y => y.Description))
                .ForMember(x => x.Price, x => x.MapFrom(y => y.Price))
                .ForMember(x => x.IsAvailabale, x => x.MapFrom(y => y.IsAvailabale))
                .ForMember(x => x.DeliveryDate, x => x.MapFrom(y => y.DeliveryDate.ToString("dd-MM-yyyy")))
                .ForMember(x => x.Unit, x => x.MapFrom(y => y.Unit))
                .ForMember(x => x.Type, x => x.MapFrom(y => y.Type))
                .ForMember(x => x.Categories, x => x.MapFrom(y => y.Categories.Select(z => z.Category)));

            CreateMap<Product, ProductDetailsVm>()
                .ForMember(x => x.ProductDescription, x => x.MapFrom(y => $"({y.Code}) {y.Description}"))
                .ForMember(x => x.Price, x => x.MapFrom(y => y.Price.ToString("C", CultureInfo.CurrentCulture)))
                .ForMember(x => x.IsAvailable, x => x.MapFrom(y => y.IsAvailabale ? "Dostnępny" : "Niedostępny"))
                .ForMember(x => x.DeliveryDate, x => x.MapFrom(y => y.DeliveryDate.ToString("dd-MM-yyyy")))
                .ForMember(x => x.CategoriesCount, x => x.MapFrom(y => y.Categories.Count()))
                .ForMember(x => x.Unit, x => x.MapFrom(y => $"({y.Unit.Code}) {y.Unit.Description}"))
                .ForMember(x => x.Type, x => x.MapFrom(y => $"({y.Type.Code}) {y.Type.Description}"));
        }

        private void MapProductUnit()
        {
            CreateMap<ProductUnit, ProductUnitVm>()
                .ForMember(x => x.Code, x => x.MapFrom(y => y.Code))
                .ForMember(x => x.Description, x => x.MapFrom(y => y.Description));
        }

        private void MapCategory()
        {
            CreateMap<Category, CategoryVm>()
                .ForMember(x => x.Code, x => x.MapFrom(y => y.Code))
                .ForMember(x => x.Description, x => x.MapFrom(y => y.Description));
        }

        private void MapProductType()
        {
            CreateMap<ProductType, ProductTypeVm>()
                .ForMember(x => x.Code, x => x.MapFrom(y => y.Code))
                .ForMember(x => x.Description, x => x.MapFrom(y => y.Description));
        }
    }
}
