﻿using SIENN.Domain.Data.Transfer.ProductType;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SIENN.Services.Interfaces
{
    public interface IProductTypeService
    {
        Task CreateAsync(CreateProductTypeCommand command);

        Task UpdateAsync(string typeCode, UpdateProductTypeCommand command);

        Task<ProductTypeVm> GetAsync(string code);

        Task<List<ProductTypeVm>> ListAsync();

        Task DeleteAsync(string typeCode);
    }
}
