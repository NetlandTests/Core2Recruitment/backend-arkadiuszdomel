﻿using SIENN.Domain.Data.Transfer.Category;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SIENN.Services.Interfaces
{
    public interface ICategoryService
    {
        Task CreateAsync(CreateCategoryCommand command);

        Task UpdateAsync(string code, UpdateCategoryCommand command);

        Task<CategoryVm> GetAsync(string code);

        Task<List<CategoryVm>> ListAsync();

        Task DeleteAsync(string code);
    }
}
