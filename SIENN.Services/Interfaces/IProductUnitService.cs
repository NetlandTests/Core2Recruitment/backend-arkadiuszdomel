﻿using SIENN.Domain.Data.Transfer.ProductUnit;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SIENN.Services.Interfaces
{
    public interface IProductUnitService
    {
        Task CreateAsync(CreateProductUnitCommand command);

        Task UpdateAsync(string unitCode, UpdateProductUnitCommand command);

        Task<ProductUnitVm> GetAsync(string code);

        Task<List<ProductUnitVm>> ListAsync();

        Task DeleteAsync(string typeCode);
    }
}
