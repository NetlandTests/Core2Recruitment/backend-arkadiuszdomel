﻿using SIENN.Domain.Data.Model;
using SIENN.Domain.Data.Transfer;
using SIENN.Domain.Data.Transfer.Product;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SIENN.Services.Interfaces
{
    public interface IProductService
    {
        Task CreateAsync(CreateProductCommand command);

        Task UpdateAsync(string code, UpdateProductCommand command);

        Task<ProductVm> GetAsync(string code);

        Task<List<ProductVm>> ListAsync();

        Task DeleteAsync(string code);

        Task<ProductDetailsVm> GetDetailsAsync(string code);

        Task<PaginationResultVm<ProductVm>> GetFilteredProductsAsync(int currentPage, int pageSize, string categoryCode, string typeCode, string unitCode);

        Task<PaginationResultVm<ProductVm>> GetOnlyAvailableProductsAsync(int currentPage, int pageSize);

    }
}
