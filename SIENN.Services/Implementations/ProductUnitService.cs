﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using SEINN.Data.EntityFramework;
using SIENN.Domain.Core;
using SIENN.Domain.Core.Exception;
using SIENN.Domain.Data.Model;
using SIENN.Domain.Data.Transfer.ProductUnit;
using SIENN.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SIENN.Services.Implementations
{
    public class ProductUnitService : IProductUnitService
    {
        private readonly SEINNDbContext _storage;
        private readonly IMapper _mapper;

        public ProductUnitService(SEINNDbContext storage, IMapper mapper)
        {
            _storage = storage;
            _mapper = mapper;
        }

        public async Task CreateAsync(CreateProductUnitCommand command)
        {
            if (await ProductUnitExists(command.Code))
            {
                throw ThrowHelper.Create(SIENNErrorCode.ProductUnitExists, "The unit with the given code exists");
            }

            var productUnit = new ProductUnit
            {
                Code = command.Code,
                Description = command.Description
            };

            _storage.ProductUnits.Add(productUnit);

            await _storage.SaveChangesAsync();
        }

        public async Task UpdateAsync(string unitCode, UpdateProductUnitCommand command)
        {
            var productUnit = await _storage.ProductUnits.FindAsync(unitCode);

            ThrowHelper.ThrowIfNull(productUnit, SIENNErrorCode.ProductUnitNotFound, "Unit not found");

            productUnit.Description = command.Description;

            await _storage.SaveChangesAsync();
        }

        public async Task<ProductUnitVm> GetAsync(string code)
        {
            var productUnit = await _storage.ProductUnits.FindAsync(code);

            ThrowHelper.ThrowIfNull(productUnit, SIENNErrorCode.ProductUnitNotFound, "Unit not found");

            return _mapper.Map<ProductUnitVm>(productUnit);
        }

        public async Task<List<ProductUnitVm>> ListAsync()
        {
            return await _storage.ProductUnits
                .ProjectTo<ProductUnitVm>(_mapper)
                .ToListAsync();
        }

        public async Task DeleteAsync(string code)
        {
            var productUnit = await _storage.ProductUnits.FindAsync(code);

            ThrowHelper.ThrowIfNull(productUnit, SIENNErrorCode.ProductUnitNotFound, "Unit not found");

            _storage.ProductUnits.Remove(productUnit);

            await _storage.SaveChangesAsync();
        }

        private async Task<bool> ProductUnitExists(string code)
        {
            return await _storage.ProductUnits.FindAsync(code) != null;
        }
    }
}
