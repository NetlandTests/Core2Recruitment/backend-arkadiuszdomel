﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using SEINN.Data.EntityFramework;
using SIENN.Domain.Core;
using SIENN.Domain.Core.Exception;
using SIENN.Domain.Data.Model;
using SIENN.Domain.Data.Transfer.ProductType;
using SIENN.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SIENN.Services.Implementations
{
    public class ProductTypeService : IProductTypeService
    {
        private readonly SEINNDbContext _storage;
        private readonly IMapper _mapper;

        public ProductTypeService(SEINNDbContext storage, IMapper mapper)
        {
            _storage = storage;
            _mapper = mapper;
        }

        public async Task CreateAsync(CreateProductTypeCommand command)
        {
            if (await ProductTypeExists(command.Code))
            {
                throw ThrowHelper.Create(SIENNErrorCode.ProductTypeExists, "The type with the given code exists");
            }

            var productType = new ProductType
            {
                Code = command.Code,
                Description = command.Description
            };

            _storage.ProductTypes.Add(productType);

            await _storage.SaveChangesAsync();
        }

        public async Task UpdateAsync(string productCode, UpdateProductTypeCommand command)
        {
            var productType = await _storage.ProductTypes.FindAsync(productCode);

            ThrowHelper.ThrowIfNull(productType, SIENNErrorCode.ProductTypeNotFound, "Type not found");

            productType.Description = command.Description;

            await _storage.SaveChangesAsync();
        }

        public async Task<ProductTypeVm> GetAsync(string code)
        {
            var productType = await _storage.ProductTypes.FindAsync(code);

            ThrowHelper.ThrowIfNull(productType, SIENNErrorCode.ProductTypeNotFound, "Type not found");

            return _mapper.Map<ProductTypeVm>(productType);
        }

        public async Task<List<ProductTypeVm>> ListAsync()
        {
            return await _storage.ProductTypes
                .ProjectTo<ProductTypeVm>(_mapper)
                .ToListAsync();
        }

        public async Task DeleteAsync(string code)
        {
            var productType = await _storage.ProductTypes.FindAsync(code);

            ThrowHelper.ThrowIfNull(productType, SIENNErrorCode.ProductTypeNotFound, "Type not found");

            _storage.ProductTypes.Remove(productType);

            await _storage.SaveChangesAsync();
        }

        private async Task<bool> ProductTypeExists(string code)
        {
            return await _storage.ProductTypes.FindAsync(code) != null;
        }

    }
}
