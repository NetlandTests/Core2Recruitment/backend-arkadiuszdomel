﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using SEINN.Data.EntityFramework;
using SIENN.Domain.Core;
using SIENN.Domain.Core.Exception;
using SIENN.Domain.Data.Model;
using SIENN.Domain.Data.Transfer;
using SIENN.Domain.Data.Transfer.Product;
using SIENN.Services.Interfaces;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SIENN.Services.Implementations
{
    public class ProductService : IProductService
    {
        private readonly SEINNDbContext _storage;
        private readonly IMapper _mapper;

        public ProductService(SEINNDbContext storage, IMapper mapper, 
            IProductTypeService productTypeService,
            IProductUnitService productUnitService)
        {
            _storage = storage;
            _mapper = mapper;
        }


        public async Task CreateAsync(CreateProductCommand command)
        {
            if(await ProductExists(command.Code))
            {
                throw ThrowHelper.Create(SIENNErrorCode.ProductExists, "The product with the given code exists");
            }

            var type = await _storage.ProductTypes.FindAsync(command.Type);

            ThrowHelper.ThrowIfNull(type, SIENNErrorCode.ProductTypeNotFound, "Type not found");

            var unit = await _storage.ProductUnits.FindAsync(command.Unit);

            ThrowHelper.ThrowIfNull(unit, SIENNErrorCode.ProductUnitNotFound, "Unit not found");

            var categories = await _storage.Categories
                .Where(x => command.Categories
                    .Distinct()
                    .Any(y => y == x.Code)
                )
                .ToListAsync();

            if(categories.Count() < command.Categories.Distinct().Count())
            {
                throw ThrowHelper.Create(SIENNErrorCode.CategoryNotFount, "Category not found");
            }


            var product = new Product
            {
                Code = command.Code,
                Description = command.Description,
                Price = command.Price,
                IsAvailabale = command.IsAvailabale,
                DeliveryDate = command.DeliveryDate,
                Type = type,
                Unit = unit,
                Categories = new List<ProductCategory>()
            };

            foreach(var category in categories)
            {
                product.Categories.Add(new ProductCategory
                {
                    Product = product,
                    Category = category
                });
            }


            _storage.Products.Add(product);

            await _storage.SaveChangesAsync();
        }

        public async Task UpdateAsync(string code, UpdateProductCommand command)
        {
            var product = await _storage.Products
                .Include(x => x.Categories)
                .Include(x => x.Type)
                .Include(x => x.Unit)
                .FirstOrDefaultAsync(x => x.Code == code);

            ThrowHelper.ThrowIfNull(product, SIENNErrorCode.ProductNotFound, "Product not found");

            var type = await _storage.ProductTypes.FindAsync(command.Type);

            ThrowHelper.ThrowIfNull(type, SIENNErrorCode.ProductTypeNotFound, "Type not found");

            var unit = await _storage.ProductUnits.FindAsync(command.Unit);

            ThrowHelper.ThrowIfNull(unit, SIENNErrorCode.ProductUnitNotFound, "Unit not found");

            var categories = await _storage.Categories
                .Where(x => command.Categories
                    .Distinct()
                    .Any(y => y == x.Code)
                )
                .ToListAsync();

            if (categories.Count() < command.Categories.Distinct().Count())
            {
                throw ThrowHelper.Create(SIENNErrorCode.CategoryNotFount, "Category not found");
            }

            product.Description = command.Description;
            product.Price = command.Price;
            product.IsAvailabale = command.IsAvailabale;
            product.DeliveryDate = command.DeliveryDate;
            product.Type = type;
            product.Unit = unit;

            foreach (var category in categories)
            {
                if(!product.Categories.Any(x => x.Category == category))
                {
                    product.Categories.Add(new ProductCategory
                    {
                        Product = product,
                        Category = category
                    });
                }
                
            }

            foreach (var productCategory in product.Categories.ToList())
            {
                if(!categories.Any(x => x == productCategory.Category))
                {
                    product.Categories.Remove(productCategory);
                }
            }

            await _storage.SaveChangesAsync();

        }

        public async Task<ProductVm> GetAsync(string code)
        {
            var product = await _storage.Products
                .Include(x => x.Categories)
                .Include(x => x.Unit)
                .Include(x => x.Type)
                .FirstOrDefaultAsync(x => x.Code == code);

            ThrowHelper.ThrowIfNull(product, SIENNErrorCode.ProductNotFound, "Product not found");

            return _mapper.Map<ProductVm>(product);
        }

        public async Task<List<ProductVm>> ListAsync()
        {
            return await _storage.Products
                .Include(x => x.Categories)
                .Include(x => x.Unit)
                .Include(x => x.Type)
                .ProjectTo<ProductVm>(_mapper)
                .ToListAsync();
        }

        public async Task DeleteAsync(string code)
        {
            var product = await _storage.Products.FindAsync(code);

            ThrowHelper.ThrowIfNull(product, SIENNErrorCode.ProductNotFound, "Product not found");

            _storage.Products.Remove(product);

            await _storage.SaveChangesAsync();
        }

        public async Task<ProductDetailsVm> GetDetailsAsync(string code)
        {
            var product = await _storage.Products
                .Include(x => x.Categories)
                .Include(x => x.Unit)
                .Include(x => x.Type)
                .FirstOrDefaultAsync(x => x.Code == code);

            ThrowHelper.ThrowIfNull(product, SIENNErrorCode.ProductNotFound, "Product not found");

            return _mapper.Map<ProductDetailsVm>(product);
        }

        public async Task<PaginationResultVm<ProductVm>> GetFilteredProductsAsync(int currentPage, int pageSize, string categoryCode, string typeCode, string unitCode)
        {
            var query = _storage.Products
                .Include(x => x.Categories)
                .Include(x => x.Unit)
                .Include(x => x.Type)
                .Where(x => (string.IsNullOrWhiteSpace(categoryCode) || x.Categories.Any(y => y.CategoryCode.ToLower() == categoryCode.ToLower())) &&
                            (string.IsNullOrWhiteSpace(typeCode) || x.Type.Code.ToLower() == typeCode.ToLower()) &&
                            (string.IsNullOrWhiteSpace(unitCode) || x.Unit.Code.ToLower() == unitCode.ToLower()));

            return await PaginateResult(currentPage, pageSize, query);
        }

        public async Task<PaginationResultVm<ProductVm>> GetOnlyAvailableProductsAsync(int currentPage, int pageSize)
        {
            var query = _storage.Products
                .Include(x => x.Categories)
                .Include(x => x.Unit)
                .Include(x => x.Type)
                .Where(x => x.IsAvailabale);

            return await PaginateResult(currentPage, pageSize, query);
        }


        private async Task<bool> ProductExists(string code)
        {
            return await _storage.Products.FindAsync(code) != null;
        }
        

        private async Task<PaginationResultVm<ProductVm>> PaginateResult(int currentPage, int pageSize, IQueryable<Product> collection)
        {
            return new PaginationResultVm<ProductVm>
            {
                Page = currentPage,
                Size = pageSize,
                TotalCount = collection.Count(),
                Items = await collection.ProjectTo<ProductVm>(_mapper).ToListAsync()
            };
        }
    }
}
