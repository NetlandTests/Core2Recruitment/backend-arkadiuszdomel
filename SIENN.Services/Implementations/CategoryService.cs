﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;
using SEINN.Data.EntityFramework;
using SIENN.Domain.Core;
using SIENN.Domain.Core.Exception;
using SIENN.Domain.Data.Model;
using SIENN.Domain.Data.Transfer.Category;
using SIENN.Services.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace SIENN.Services.Implementations
{
    public class CategoryService : ICategoryService
    {
        private readonly SEINNDbContext _storage;
        private readonly IMapper _mapper;

        public CategoryService(SEINNDbContext storage, IMapper mapper)
        {
            _storage = storage;
            _mapper = mapper;
        }

        public async Task CreateAsync(CreateCategoryCommand command)
        {
            if (await CategoryExists(command.Code))
            {
                throw ThrowHelper.Create(SIENNErrorCode.CategoryExists, "The category with the given code exists");
            }

            var category = new Category
            {
                Code = command.Code,
                Description = command.Description
            };

            _storage.Categories.Add(category);

            await _storage.SaveChangesAsync();
        }

        public async Task UpdateAsync(string code, UpdateCategoryCommand command)
        {
            var category = await _storage.Categories.FindAsync(code);

            ThrowHelper.ThrowIfNull(category, SIENNErrorCode.CategoryNotFount, "Category not found");

            category.Description = command.Description;

            await _storage.SaveChangesAsync();
        }

        public async Task<CategoryVm> GetAsync(string code)
        {
            var category = await _storage.Categories.FindAsync(code);

            ThrowHelper.ThrowIfNull(category, SIENNErrorCode.CategoryNotFount, "Category not found");

            return _mapper.Map<CategoryVm>(category);
        }

        public async Task<List<CategoryVm>> ListAsync()
        {
            return await _storage.Categories
                .ProjectTo<CategoryVm>(_mapper)
                .ToListAsync();
        }

        public async Task DeleteAsync(string code)
        {
            var category = await _storage.Categories.FindAsync(code);

            ThrowHelper.ThrowIfNull(category, SIENNErrorCode.CategoryNotFount, "Category not found");

            _storage.Categories.Remove(category);

            await _storage.SaveChangesAsync();
        }

        private async Task<bool> CategoryExists(string code)
        {
            return await _storage.Categories.FindAsync(code) != null;
        }
    }
}
